(function($){
	$('[data-toggle="tooltip"]').tooltip();

	// $(window).scroll(function(){
	//     if ($(window).scrollTop() >= 72) {
	//         $('.nav-seting').addClass('fixed-nav');
	//     }
	//     else {
	//         $('.nav-seting').removeClass('fixed-nav');
	//     }
	// });
	$('.js-link').on('click', function(){
		$(this).parents('.nav').find('.nav-item').removeClass('active');
		$(this).parent('li').addClass('active');
	});
	$('#table_id, #table_id2').DataTable();
	$('a[href*="#"].js-link').click(function() {
	    //var that = $(this);
	    $('html,body, .setting-wrapper').animate({
	      scrollTop: $( $.attr(this, 'href') ).offset().top - 20
	    }, 1000);
	    return false;
	 });
	window.onscroll = function() {myFunction()};

	var header = document.getElementById("myNav");
	var sticky = header.offsetTop;

	function myFunction() {
	  if (window.pageYOffset > sticky) {
	    header.classList.add("sticky");
	  } else {
	    header.classList.remove("sticky");
	  }
	}

})(jQuery);